from persistent.mapping import PersistentMapping
from .camera import CameraGphoto2


def appmaker(zodb_root):
    if 'app_root' not in zodb_root:
        app_root = CameraGphoto2()
        zodb_root['app_root'] = app_root
    return zodb_root['app_root']
