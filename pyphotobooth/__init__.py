from pyramid.config import Configurator
from pyramid_zodbconn import get_connection
from pyramid.settings import asbool
from .models import appmaker


def root_factory(request):
    conn = get_connection(request)
    return appmaker(conn.root())


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    with Configurator(settings=settings) as config:
        settings["tm.manager_hook"] = "pyramid_tm.explicit_manager"
        pyphotobooth_picture_rotation = settings.get("pyphotobooth.picture_rotation")
        settings["pyphotobooth.picture_rotation"] = pyphotobooth_picture_rotation
        config.include("pyramid_tm")
        config.include("pyramid_retry")
        config.include("pyramid_zodbconn")
        config.set_root_factory(root_factory)
        config.include("pyramid_chameleon")
        config.include(".gphoto_connection")
        config.include(".routes")
        config.scan()
    return config.make_wsgi_app()
