import os


def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_static_view('static_featherlight', 'node_modules/featherlight/', cache_max_age=3600)
    config.add_static_view(name='pics', path=os.path.expanduser("~/pyphotobooth_data/"))
    config.add_static_view(name='thumbs', path=os.path.expanduser("~/pyphotobooth_data_thumbs/"))


