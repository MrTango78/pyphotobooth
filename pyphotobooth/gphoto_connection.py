from PIL import Image
from PIL import ImageOps
import gphoto2 as gp
import io
import logging
import os


def includeme(config):
    pyphotobooth_pictures_dir = os.path.expanduser("~/pyphotobooth_data/")
    pyphotobooth_thumbs_dir = os.path.expanduser("~/pyphotobooth_data_thumbs/")
    dirname = os.path.dirname(pyphotobooth_pictures_dir)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    dirname = os.path.dirname(pyphotobooth_thumbs_dir)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    config.add_settings({
        'pyphotobooth_pictures_dir': pyphotobooth_pictures_dir,
        'pyphotobooth_thumbs_dir': pyphotobooth_thumbs_dir,
    })
    cap = CameraGphoto2()
    config.registry._gphoto_connection = cap


class CameraGphoto2(object):
    def __init__(self):
        self.connected = False
        self._setup_logging()
        self._setup_camera()

    def _setup_logging(self):
        gp.error_severity[gp.GP_ERROR] = logging.ERROR
        gp.check_result(gp.use_python_logging())

    def _setup_camera(self):
        if not gp.gp_camera_autodetect()[0]:
            logging.warn("No camera attached!")
            return
        self._ctxt = gp.Context()
        self._cap = gp.Camera()
        # self._cap.exit(self._ctxt)
        self._cap.init(self._ctxt)
        self.connected = True

        logging.info("Camera summary: %s", str(self._cap.get_summary(self._ctxt)))

        # read model specific configuration
        # config = self._cap.get_config()
        # self.loadConfig(config.get_child_by_name("cameramodel").get_value())

        # set startup configuration
        # self._changeConfig('Startup')

        #  print current config
        # self._print_config(self._cap.get_config())

    def check_connection(self):
        self._setup_camera()

    @staticmethod
    def _print_config(config):
        config_txt = "Camera configuration:\n"
        config_txt += CameraGphoto2._configTreeToText(config)
        logging.info(config_txt)

    def get_preview(self):
        camera_file = self._cap.capture_preview()
        file_data = camera_file.get_data_and_size()
        return Image.open(io.BytesIO(file_data))

    def get_picture(self):
        file_path = self._cap.capture(gp.GP_CAPTURE_IMAGE)
        camera_file = self._cap.file_get(
            file_path.folder, file_path.name, gp.GP_FILE_TYPE_NORMAL
        )
        file_data = camera_file.get_data_and_size()
        return Image.open(io.BytesIO(file_data))

    @staticmethod
    def _configTreeToText(tree, indent=0):

        config_txt = ''

        for chld in tree.get_children():
            config_txt += indent * ' '
            config_txt += chld.get_label() + ' [' + chld.get_name() + ']: '

            if chld.count_children() > 0:
                config_txt += '\n'
                config_txt += CameraGphoto2._configTreeToText(chld, indent + 4)
            else:
                config_txt += str(chld.get_value())
                try:
                    choice_txt = ' ('

                    for c in chld.get_choices():
                        choice_txt += c + ', '

                    choice_txt += ')'
                    config_txt += choice_txt
                except gp.GPhoto2Error:
                    pass
                config_txt += '\n'

        return config_txt