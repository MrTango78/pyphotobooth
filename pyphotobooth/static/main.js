jQuery(document).ready(function($){
    $('a.gallery').featherlightGallery({
        previousIcon: '«',
        nextIcon: '»',
        galleryFadeIn: 300,
        openSpeed: 300
    });

    if ($('.countdown_wrapper').length != 0) {
        var urlPath = location.protocol + "//" + location.host + "/shot";
        $.get(urlPath)
            .done(function(data) {
                $nav = $(data).find(".nav");
                $content = $(data).find(".content");
                console.log($content)
                $(".nav").html($nav);
                $(".content").html($content);
                window.history.pushState({"html":data.html,"pageTitle":data.pageTitle},"", urlPath);
            });
        var timer = 2;
        $('.countdown').text(timer);
        var interval = setInterval(function() {
            timer--;
            $('.countdown').text(timer);
            if (timer === 0) {
                clearInterval(interval);
                $('.countdown').text("Cheeees!");
            }
        }, 1000);
    }
});