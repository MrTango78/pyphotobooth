from PIL import Image
from ..models.camera import CameraGphoto2
from datetime import datetime
from pyramid.response import Response
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
from pyramid import threadlocal
import cups
import logging
import io
import os
import base64


ROTATIONS = {0: None, 90: Image.ROTATE_90, 180: Image.ROTATE_180, 270: Image.ROTATE_270}


@view_config(context=CameraGphoto2, renderer="../templates/home.pt")
class HomeView(object):
    def __init__(self, request):
        self.request = request

    def __call__(self):
        return {}


@view_config(name="preview", context=CameraGphoto2, renderer="../templates/camera.pt")
class CameraPreviewView(object):
    def __init__(self, request):
        self.request = request
        self.preview = None
        self.current_file_path = None
        registry = threadlocal.get_current_registry()
        self.pictures_dir = registry.settings["pyphotobooth_pictures_dir"]
        self.thumbs_dir = registry.settings["pyphotobooth_thumbs_dir"]
        self.picture_rotation = ROTATIONS[
            int(registry.settings["pyphotobooth.picture_rotation"])
        ]
        self._cap = self.request.registry._gphoto_connection
        # self.findExistingFiles()

    def __call__(self):
        self.preview = self.capture_preview()
        return {"preview": self.preview, "current_file_path": self.current_file_path}

    def capture_preview(self):
        self._cap.check_connection()
        picture = self._cap.get_preview()
        # if self._rotation is not None:
        # picture = picture.resize((900, 900))
        if self.picture_rotation:
            picture = picture.transpose(self.picture_rotation)
        picture.thumbnail((800, 800), Image.ANTIALIAS)
        # picture = ImageOps.mirror(picture)
        byte_data = io.BytesIO()
        picture.save(byte_data, format="jpeg")
        b64_data = base64.b64encode(byte_data.getvalue()).decode("ascii")
        return b64_data


@view_config(
    name="countdown", context=CameraGphoto2, renderer="../templates/countdown.pt"
)
class CountdownView(object):
    def __init__(self, request):
        self.request = request

    def __call__(self):
        return {}


@view_config(name="shot", context=CameraGphoto2, renderer="../templates/camera.pt")
class CameraShotView(object):
    def __init__(self, request):
        self.request = request
        self.preview = None
        registry = threadlocal.get_current_registry()
        self.pictures_dir = registry.settings["pyphotobooth_pictures_dir"]
        self.thumbs_dir = registry.settings["pyphotobooth_thumbs_dir"]
        self.picture_rotation = ROTATIONS[
            int(registry.settings["pyphotobooth.picture_rotation"])
        ]
        self._cap = self.request.registry._gphoto_connection

    def __call__(self):
        self.preview = self.capture_picture()
        return {"preview": self.preview, "current_file_path": self.current_file_path}

    def capture_picture(self):
        self._cap.check_connection()
        picture = self._cap.get_picture()
        if self.picture_rotation:
            picture = picture.transpose(self.picture_rotation)
        thumb = picture.copy()
        picture_data = io.BytesIO()
        picture.save(picture_data, format="jpeg")
        thumb.thumbnail((1024, 1024), Image.ANTIALIAS)
        thumb_data = io.BytesIO()
        picture.save(thumb_data, format="jpeg")
        self.current_file_path = self.save_picture(picture_data, thumb_data)
        b64_data = base64.b64encode(picture_data.getvalue()).decode("ascii")
        return b64_data

    def save_picture(self, picture, thumb):
        filename = "{}.jpg".format(datetime.now().isoformat())
        filepath = os.path.join(self.pictures_dir, filename)
        thumb_filepath = os.path.join(self.thumbs_dir, filename)
        logging.info("Saving picture as %s", filepath)
        with open(filepath, "wb") as f:
            f.write(picture.getbuffer())
        logging.info("Saving thumb as %s", thumb_filepath)
        with open(thumb_filepath, "wb") as f:
            f.write(thumb.getbuffer())
        return filepath


@view_config(name="gallery", context=CameraGphoto2, renderer="../templates/gallery.pt")
class GalleryView(object):
    def __init__(self, request):
        self.request = request
        registry = threadlocal.get_current_registry()
        self.pictures_dir = registry.settings["pyphotobooth_pictures_dir"]
        self.thumbs_dir = registry.settings["pyphotobooth_thumbs_dir"]

    def __call__(self):
        picture_paths = self.find_existing_pictures()
        return {"picture_paths": picture_paths, "pictures_dir": self.pictures_dir}

    def find_existing_pictures(self):
        pictures = os.listdir(path=self.thumbs_dir)
        picture_paths = [os.path.join("/thumbs", pic) for pic in pictures]
        return sorted(picture_paths, reverse=True)


@view_config(name="print", context=CameraGphoto2, renderer="../templates/camera.pt")
class PrintView(object):
    def __init__(self, request):
        self.request = request
        self.preview = None
        self.cups_conn = cups.Connection()

    def __call__(self):
        path = self.request.params.get("path")
        printer = self.cups_conn.getDefault()
        self.cups_conn.printFile(printer, path, "PyPhotobooth", {})
        return HTTPFound(location=self.request.host_url)

